# Judahnator Proxmox Library

## Installation

```bash
composer require judahnator/proxmox
```

## Usage

Set up a class instance first. You will need your Proxmox hostname, username, and token.

```php
use Judahnator\Proxmox\Proxmox;

$proxmox = new Proxmox(
    $hostname,
    $username,
    $token_name,
    $token_value,
);
```

This class has four methods used to interact with the API. They are `delete()|get()|post()|put()`. Each takes three arguments: an `ApiEndpoint`, url arguments, and the request body. 

### Examples

```php
use Judahnator\Proxmox\ApiPaths;

// Get the PVE version
$proxmox->get(ApiPaths::VERSION);

// Clone a container
$proxmox->post(
    ApiPaths::NODES_NODE_QEMU_VMID_CLONE,   // The API endpoint
    ['node' => 'pve', 'vmid' => 9000],      // The URL parameters
    ['newid' => 9001],                      // The request body
);
```