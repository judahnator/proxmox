<?php declare(strict_types=1);

namespace Judahnator\Proxmox;

use Symfony\Contracts\HttpClient\HttpClientInterface;

final class HttpClient
{
    public function __construct(
        private readonly string $hostname,
        private readonly int $port,
        private readonly HttpClientInterface $httpClient,
    ) {}

    public function request(HttpMethod $method, string $endpoint, array $data): array
    {
        return $this->httpClient->request(
            $method->name,
            sprintf('https://%s:%d/api2/json/%s', $this->hostname, $this->port, ltrim($endpoint, '/')),
            ['body' => $data]
        )->toArray();
    }
}