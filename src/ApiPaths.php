<?php declare(strict_types=1);

namespace Judahnator\Proxmox;

/**
 * @see https://pve.proxmox.com/pve-docs/api-viewer/index.html
 */
enum ApiPaths: string
{
    case CLUSTER = '/cluster';

    case CLUSTER_ACME = '/cluster/acme';
    case CLUSTER_ACME_ACCOUNT = '/cluster/acme/account';
    case CLUSTER_ACME_ACCOUNT_NAME = '/cluster/acme/account/{name}';
    case CLUSTER_ACME_PLUGINS = '/cluster/acme/plugins';
    case CLUSTER_ACME_PLUGINS_ID = '/cluster/acme/plugins/{id}';
    case CLUSTER_ACME_CHALLENGE_SCHEMA = '/cluster/acme/challenge-schema';
    case CLUSTER_ACME_DIRECTORIES = '/cluster/acme/directories';
    case CLUSTER_ACME_TOS = '/cluster/acme/tos';

    case CLUSTER_BACKUP = '/cluster/backup';
    case CLUSTER_BACKUP_ID = '/cluster/backup/{id}';
    case CLUSTER_BACKUP_ID_INCLUDED_VOLUMES = '/cluster/backup/{id}/included_volumes';
    case CLUSTER_BACKUP_INFO = '/cluster/backup-info';
    case CLUSTER_BACKUP_INFO_NOT_BACKED_UP = '/cluster/backup-info/not-backed-up';

    case CLUSTER_CEPH = '/cluster/ceph';
    case CLUSTER_CEPH_FLAGS = '/cluster/ceph/flags';
    case CLUSTER_CEPH_FLAGS_FLAG = '/cluster/ceph/flags/{flag}';
    case CLUSTER_CEPH_METADATA = '/cluster/ceph/metadata';
    case CLUSTER_CEPH_STATUS = '/cluster/ceph/status';

    case CLUSTER_CONFIG = '/cluster/config';
    case CLUSTER_CONFIG_NODES = '/cluster/config/nodes';
    case CLUSTER_CONFIG_NODES_NODE = '/cluster/config/nodes/{node}';
    case CLUSTER_CONFIG_APIVERSION = '/cluster/config/apiversion';
    case CLUSTER_CONFIG_JOIN = '/cluster/config/join';
    case CLUSTER_CONFIG_QDEVICE = '/cluster/config/qdevice';
    case CLUSTER_CONFIG_TOTEM = '/cluster/config/totem';

    case CLUSTER_FIREWALL = '/cluster/firewall';
    case CLUSTER_FIREWALL_ALIASES = '/cluster/firewall/aliases';
    case CLUSTER_FIREWALL_ALIASES_NAME = '/cluster/firewall/aliases/{name}';
    case CLUSTER_FIREWALL_GROUPS = '/cluster/firewall/groups';
    case CLUSTER_FIREWALL_GROUPS_GROUP = '/cluster/firewall/groups/{group}';
    case CLUSTER_FIREWALL_GROUPS_GROUP_POS = '/cluster/firewall/groups/{group}/{pos}';
    case CLUSTER_FIREWALL_IPSET = '/cluster/firewall/ipset';
    case CLUSTER_FIREWALL_IPSET_NAME = '/cluster/firewall/ipset/{name}';
    case CLUSTER_FIREWALL_IPSET_NAME_CIDR = '/cluster/firewall/ipset/{name}/{cidr}';
    case CLUSTER_FIREWALL_RULES = '/cluster/firewall/rules';
    case CLUSTER_FIREWALL_RULES_POS = '/cluster/firewall/rules/{pos}';
    case CLUSTER_FIREWALL_MACROS = '/cluster/firewall/macros';
    case CLUSTER_FIREWALL_OPTIONS = '/cluster/firewall/options';
    case CLUSTER_FIREWALL_REFS = '/cluster/firewall/refs';

    case CLUSTER_HA = '/cluster/ha';
    case CLUSTER_HA_GROUPS = '/cluster/ha/groups';
    case CLUSTER_HA_GROUPS_GROUP = '/cluster/ha/groups/{group}';
    case CLUSTER_HA_RESOURCES = '/cluster/ha/resources';
    case CLUSTER_HA_RESOURCES_SID = '/cluster/ha/resources/{sid}';
    case CLUSTER_HA_RESOURCES_SID_MIGRATE = '/cluster/ha/resources/{sid}/migrate';
    case CLUSTER_HA_RESOURCES_SID_RELOCATE = '/cluster/ha/resources/{sid}/relocate';
    case CLUSTER_HA_STATUS = '/cluster/ha/status';
    case CLUSTER_HA_STATUS_CURRENT = '/cluster/ha/status/current';
    case CLUSTER_HA_STATUS_MANAGER_STATUS = '/cluster/ha/status/manager_status';

    case CLUSTER_JOBS = '/cluster/jobs';
    case CLUSTER_JOBS_SCHEDULE_ANALYZE = '/cluster/jobs/schedule-analyze';

    case CLUSTER_METRICS = '/cluster/metrics';
    case CLUSTER_METRICS_SERVER = '/cluster/metrics/server';
    case CLUSTER_METRICS_SERVER_ID = '/cluster/metrics/server/{id}';

    case CLUSTER_REPLICATION = '/cluster/replication';
    case CLUSTER_REPLICATION_ID = '/cluster/replication/{id}';

    case CLUSTER_SDN = '/cluster/sdn';
    case CLUSTER_SDN_CONTROLLERS = '/cluster/sdn/controllers';
    case CLUSTER_SDN_CONTROLLERS_CONTROLLER = '/cluster/sdn/controllers/{controller}';
    case CLUSTER_SDN_DNS = '/cluster/sdn/dns';
    case CLUSTER_SDN_DNS_DNS = '/cluster/sdn/dns/{dns}';
    case CLUSTER_SDN_IPAMS = '/cluster/sdn/ipams';
    case CLUSTER_SDN_IPAMS_IPAM = '/cluster/sdn/ipams/{ipam}';
    case CLUSTER_SDN_VNETS = '/cluster/sdn/vnets';
    case CLUSTER_SDN_VNETS_VNET = '/cluster/sdn/vnets/{vnet}';
    case CLUSTER_SDN_VNETS_VNET_SUBNETS = '/cluster/sdn/vnets/{vnet}/subnets';
    case CLUSTER_SDN_VNETS_VNET_SUBNETS_SUBNET = '/cluster/sdn/vnets/{vnet}/subnets/{subnet}';
    case CLUSTER_SDN_ZONES = '/cluster/sdn/zones';
    case CLUSTER_SDN_ZONES_ZONE = '/cluster/sdn/zones/{zone}';

    case CLUSTER_LOG = '/cluster/log';
    case CLUSTER_NEXTID = '/cluster/nextid';
    case CLUSTER_OPTIONS = '/cluster/options';
    case CLUSTER_RESOURCES = '/cluster/resources';
    case CLUSTER_STATUS = '/cluster/status';
    case CLUSTER_TASKS = '/cluster/tasks';

    // ---

    case NODES = '/nodes';

    case NODES_NODE = '/nodes/{node}'; // index

    case NODES_NODE_APT = '/nodes/{node}/apt';
    case NODES_NODE_APT_CHANGELOG = '/nodes/{node}/apt/changelog';
    case NODES_NODE_APT_REPOSITORIES = '/nodes/{node}/apt/repositories';
    case NODES_NODE_APT_UPDATE = '/nodes/{node}/apt/update';
    case NODES_NODE_APT_VERSIONS = '/nodes/{node}/apt/versions';

    case NODES_NODE_CAPABILITIES = '/nodes/{node}/capabilities';
    case NODES_NODE_CAPABILITIES_QEMU = '/nodes/{node}/capabilities/qemu';
    case NODES_NODE_CAPABILITIES_QEMU_CPU = '/nodes/{node}/capabilities/qemu/cpu';
    case NODES_NODE_CAPABILITIES_QEMU_MACHINES = '/nodes/{node}/capabilities/qemu/machines';

    case NODES_NODE_CEPH = '/nodes/{node}/ceph';
    case NODES_NODE_CEPH_FS = '/nodes/{node}/ceph/fs';
    case NODES_NODE_CEPH_FS_NAME = '/nodes/{node}/ceph/fs/{name}';
    case NODES_NODE_CEPH_MDS = '/nodes/{node}/ceph/mds';
    case NODES_NODE_CEPH_MDS_NAME = '/nodes/{node}/ceph/mds/{name}';
    case NODES_NODE_CEPH_MGR = '/nodes/{node}/ceph/mgr';
    case NODES_NODE_CEPH_MGR_ID = '/nodes/{node}/ceph/mgr/{id}';

    case NODES_NODE_CERTIFICATES = '/nodes/{node}/certificates';
    case NODES_NODE_CERTIFICATES_ACME = '/nodes/{node}/certificates/acme';
    case NODES_NODE_CERTIFICATES_ACME_CERTIFICATE = '/nodes/{node}/certificates/acme/certificate';
    case NODES_NODE_CERTIFICATES_CUSTOM = '/nodes/{node}/certificates/custom';
    case NODES_NODE_CERTIFICATES_INFO = '/nodes/{node}/certificates/info';

    case NODES_NODE_DISKS = '/nodes/{node}/disks';
    case NODES_NODE_DISKS_DIRECTORY = '/nodes/{node}/disks/directory';
    case NODES_NODE_DISKS_DIRECTORY_NAME = '/nodes/{node}/disks/directory/{name}';
    case NODES_NODE_DISKS_LVM = '/nodes/{node}/disks/lvm';
    case NODES_NODE_DISKS_LVM_NAME = '/nodes/{node}/disks/lvm/{name}';
    case NODES_NODE_DISKS_LVMTHIN = '/nodes/{node}/disks/lvmthin';
    case NODES_NODE_DISKS_LVMTHIN_NAME = '/nodes/{node}/disks/lvmthin/{name}';
    case NODES_NODE_DISKS_ZFS = '/nodes/{node}/disks/zfs';
    case NODES_NODE_DISKS_ZFS_NAME = '/nodes/{node}/disks/zfs/{name}';
    case NODES_NODE_DISKS_INITGPT = '/nodes/{node}/disks/initgpt';
    case NODES_NODE_DISKS_LIST = '/nodes/{node}/disks/list';
    case NODES_NODE_DISKS_SMART = '/nodes/{node}/disks/smart';
    case NODES_NODE_DISKS_WIPEDISK = '/nodes/{node}/disks/wipedisk';

    // todo firewall

    // todo hardware

    case NODES_NODE_LXC = '/nodes/{node}/lxc';
    case NODES_NODE_LXC_VMID = '/nodes/{node}/lxc/{vmid}';
    case NODES_NODE_LXC_VMID_FIREWALL = '/nodes/{node}/lxc/{vmid}/firewall';
    case NODES_NODE_LXC_VMID_FIREWALL_ALIASES = '/nodes/{node}/lxc/{vmid}/firewall/aliases';
    case NODES_NODE_LXC_VMID_FIREWALL_ALIASES_NAME = '/nodes/{node}/lxc/{vmid}/firewall/aliases/{name}';
    case NODES_NODE_LXC_VMID_FIREWALL_IPSET = '/nodes/{node}/lxc/{vmid}/firewall/ipset';
    case NODES_NODE_LXC_VMID_FIREWALL_IPSET_NAME = '/nodes/{node}/lxc/{vmid}/firewall/ipset/{name}';
    case NODES_NODE_LXC_VMID_FIREWALL_RULES = '/nodes/{node}/lxc/{vmid}/firewall/rules';
    case NODES_NODE_LXC_VMID_FIREWALL_RULES_POS = '/nodes/{node}/lxc/{vmid}/firewall/rules/{pos}';
    case NODES_NODE_LXC_VMID_FIREWALL_LOG = '/nodes/{node}/lxc/{vmid}/firewall/log';
    case NODES_NODE_LXC_VMID_FIREWALL_OPTIONS = '/nodes/{node}/lxc/{vmid}/firewall/options';
    case NODES_NODE_LXC_VMID_FIREWALL_REFS = '/nodes/{node}/lxc/{vmid}/firewall/refs';
    case NODES_NODE_LXC_VMID_SNAPSHOT = '/nodes/{node}/lxc/{vmid}/snapshot';
    case NODES_NODE_LXC_VMID_SNAPSHOT_SNAPNAME = '/nodes/{node}/lxc/{vmid}/snapshot/{snapname}';
    case NODES_NODE_LXC_VMID_SNAPSHOT_SNAPNAME_CONFIG = '/nodes/{node}/lxc/{vmid}/snapshot/{snapname}/config';
    case NODES_NODE_LXC_VMID_SNAPSHOT_SNAPNAME_ROLLBACK = '/nodes/{node}/lxc/{vmid}/snapshot/{snapname}/rollback';
    case NODES_NODE_LXC_VMID_STATUS = '/nodes/{node}/lxc/{vmid}/status';
    case NODES_NODE_LXC_VMID_STATUS_CURRENT = '/nodes/{node}/lxc/{vmid}/status/current';
    case NODES_NODE_LXC_VMID_STATUS_REBOOT = '/nodes/{node}/lxc/{vmid}/status/reboot';
    case NODES_NODE_LXC_VMID_STATUS_RESUME = '/nodes/{node}/lxc/{vmid}/status/resume';
    case NODES_NODE_LXC_VMID_STATUS_SHUTDOWN = '/nodes/{node}/lxc/{vmid}/status/shutdown';
    case NODES_NODE_LXC_VMID_STATUS_START = '/nodes/{node}/lxc/{vmid}/status/start';
    case NODES_NODE_LXC_VMID_STATUS_STOP = '/nodes/{node}/lxc/{vmid}/status/stop';
    case NODES_NODE_LXC_VMID_STATUS_SUSPEND = '/nodes/{node}/lxc/{vmid}/status/suspend';
    case NODES_NODE_LXC_VMID_CLONE = '/nodes/{node}/lxc/{vmid}/clone';
    case NODES_NODE_LXC_VMID_CONFIG = '/nodes/{node}/lxc/{vmid}/config';
    case NODES_NODE_LXC_VMID_FEATURE = '/nodes/{node}/lxc/{vmid}/feature';
    case NODES_NODE_LXC_VMID_MIGRATE = '/nodes/{node}/lxc/{vmid}/migrate';
    case NODES_NODE_LXC_VMID_MOVE_VOLUME = '/nodes/{node}/lxc/{vmid}/move_volume';
    case NODES_NODE_LXC_VMID_PENDING = '/nodes/{node}/lxc/{vmid}/pending';
    case NODES_NODE_LXC_VMID_RESIZE = '/nodes/{node}/lxc/{vmid}/resize';
    case NODES_NODE_LXC_VMID_RRD = '/nodes/{node}/lxc/{vmid}/rrd';
    case NODES_NODE_LXC_VMID_RRDDATA = '/nodes/{node}/lxc/{vmid}/rrddata';
    case NODES_NODE_LXC_VMID_SPICEPROXY = '/nodes/{node}/lxc/{vmid}/spiceproxy';
    case NODES_NODE_LXC_VMID_TEMPLATE = '/nodes/{node}/lxc/{vmid}/template';
    case NODES_NODE_LXC_VMID_TERMPROXY = '/nodes/{node}/lxc/{vmid}/termproxy';
    case NODES_NODE_LXC_VMID_VNCPROXY = '/nodes/{node}/lxc/{vmid}/vncproxy';
    case NODES_NODE_LXC_VMID_VNCWEBSOCKET = '/nodes/{node}/lxc/{vmid}/vncwebsocket';

    // todo network

    case NODES_NODE_QEMU = '/nodes/{node}/qemu';
    case NODES_NODE_QEMU_VMID = '/nodes/{node}/qemu/{vmid}';
    case NODES_NODE_QEMU_VMID_AGENT = '/nodes/{node}/qemu/{vmid}/agent';
    case NODES_NODE_QEMU_VMID_AGENT_EXEC = '/nodes/{node}/qemu/{vmid}/agent/exec';
    case NODES_NODE_QEMU_VMID_AGENT_EXEC_STATUS = '/nodes/{node}/qemu/{vmid}/agent/exec-status';
    case NODES_NODE_QEMU_VMID_AGENT_FILE_READ = '/nodes/{node}/qemu/{vmid}/agent/file-read';
    case NODES_NODE_QEMU_VMID_AGENT_FILE_WRITE = '/nodes/{node}/qemu/{vmid}/agent/file-write';
    case NODES_NODE_QEMU_VMID_AGENT_FSFREEZE_FREEZE = '/nodes/{node}/qemu/{vmid}/agent/fsfreeze-freeze';
    case NODES_NODE_QEMU_VMID_AGENT_FSFREEZE_STATUS = '/nodes/{node}/qemu/{vmid}/agent/fsfreeze-status';
    case NODES_NODE_QEMU_VMID_AGENT_FSFREEZE_THAW = '/nodes/{node}/qemu/{vmid}/agent/fsfreeze-thaw';
    case NODES_NODE_QEMU_VMID_AGENT_FSTRIM = '/nodes/{node}/qemu/{vmid}/agent/fstrim';
    case NODES_NODE_QEMU_VMID_AGENT_GET_FSINFO = '/nodes/{node}/qemu/{vmid}/agent/get-fsinfo';
    case NODES_NODE_QEMU_VMID_AGENT_GET_HOST_NAME = '/nodes/{node}/qemu/{vmid}/agent/get-host-name';
    case NODES_NODE_QEMU_VMID_AGENT_GET_MEMORY_BLOCKS_INFO = '/nodes/{node}/qemu/{vmid}/agent/get-memory-block-info';
    case NODES_NODE_QEMU_VMID_AGENT_GET_MEMORY_BLOCKS = '/nodes/{node}/qemu/{vmid}/agent/get-memory-blocks';
    case NODES_NODE_QEMU_VMID_AGENT_GET_OSINFO = '/nodes/{node}/qemu/{vmid}/agent/get-osinfo';
    case NODES_NODE_QEMU_VMID_AGENT_GET_TIME = '/nodes/{node}/qemu/{vmid}/agent/get-time';
    case NODES_NODE_QEMU_VMID_AGENT_GET_TIMEZONE = '/nodes/{node}/qemu/{vmid}/agent/get-timezone';
    case NODES_NODE_QEMU_VMID_AGENT_GET_USERS = '/nodes/{node}/qemu/{vmid}/agent/get-users';
    case NODES_NODE_QEMU_VMID_AGENT_GET_VCPUS = '/nodes/{node}/qemu/{vmid}/agent/get-vcpus';
    case NODES_NODE_QEMU_VMID_AGENT_INFO = '/nodes/{node}/qemu/{vmid}/agent/info';
    case NODES_NODE_QEMU_VMID_AGENT_NETWORK_GET_INTERFACES = '/nodes/{node}/qemu/{vmid}/agent/network-get-interfaces';
    case NODES_NODE_QEMU_VMID_AGENT_PING = '/nodes/{node}/qemu/{vmid}/agent/ping';
    case NODES_NODE_QEMU_VMID_AGENT_SET_USER_PASSWORD = '/nodes/{node}/qemu/{vmid}/agent/set-user-password';
    case NODES_NODE_QEMU_VMID_AGENT_SHUTDOWN = '/nodes/{node}/qemu/{vmid}/agent/shutdown';
    case NODES_NODE_QEMU_VMID_AGENT_SUSPEND_DISK = '/nodes/{node}/qemu/{vmid}/agent/suspend-disk';
    case NODES_NODE_QEMU_VMID_AGENT_SUSPEND_HYBRID = '/nodes/{node}/qemu/{vmid}/agent/suspend-hybrid';
    case NODES_NODE_QEMU_VMID_AGENT_SUSPEND_RAM = '/nodes/{node}/qemu/{vmid}/agent/suspend-ram';
    case NODES_NODE_QEMU_VMID_CLOUDINIT_DUMP = '/nodes/{node}/qemu/{vmid}/cloudinit/dump';
    case NODES_NODE_QEMU_VMID_FIREWALL = '/nodes/{node}/qemu/{vmid}/firewall';
    case NODES_NODE_QEMU_VMID_FIREWALL_ALIAS = '/nodes/{node}/qemu/{vmid}/firewall/aliases';
    case NODES_NODE_QEMU_VMID_FIREWALL_ALIAS_NAME = '/nodes/{node}/qemu/{vmid}/firewall/aliases/{name}';
    case NODES_NODE_QEMU_VMID_FIREWALL_IPSET = '/nodes/{node}/qemu/{vmid}/firewall/ipset';
    case NODES_NODE_QEMU_VMID_FIREWALL_IPSET_NAME = '/nodes/{node}/qemu/{vmid}/firewall/ipset/{name}';
    case NODES_NODE_QEMU_VMID_FIREWALL_IPSET_NAME_CIDR = '/nodes/{node}/qemu/{vmid}/firewall/ipset/{name}/{cidr}';
    case NODES_NODE_QEMU_VMID_FIREWALL_RULES = '/nodes/{node}/qemu/{vmid}/firewall/rules';
    case NODES_NODE_QEMU_VMID_FIREWALL_RULES_POS = '/nodes/{node}/qemu/{vmid}/firewall/rules/{pos}';
    case NODES_NODE_QEMU_VMID_FIREWALL_LOG = '/nodes/{node}/qemu/{vmid}/firewall/log';
    case NODES_NODE_QEMU_VMID_FIREWALL_OPTIONS = '/nodes/{node}/qemu/{vmid}/firewall/options';
    case NODES_NODE_QEMU_VMID_FIREWALL_REFS = '/nodes/{node}/qemu/{vmid}/firewall/refs';
    case NODES_NODE_QEMU_VMID_SNAPSHOT = '/nodes/{node}/qemu/{vmid}/snapshot';
    case NODES_NODE_QEMU_VMID_SNAPSHOT_SNAPNAME = '/nodes/{node}/qemu/{vmid}/snapshot/{snapname}';
    case NODES_NODE_QEMU_VMID_SNAPSHOT_SNAPNAME_CONFIG = '/nodes/{node}/qemu/{vmid}/snapshot/{snapname}/config';
    case NODES_NODE_QEMU_VMID_SNAPSHOT_SNAPNAME_ROLLBACK = '/nodes/{node}/qemu/{vmid}/snapshot/{snapname}/rollback';
    case NODES_NODE_QEMU_VMID_STATUS = '/nodes/{node}/qemu/{vmid}/status';
    case NODES_NODE_QEMU_VMID_STATUS_CURRENT = '/nodes/{node}/qemu/{vmid}/status/current';
    case NODES_NODE_QEMU_VMID_STATUS_REBOOT = '/nodes/{node}/qemu/{vmid}/status/reboot';
    case NODES_NODE_QEMU_VMID_STATUS_RESET = '/nodes/{node}/qemu/{vmid}/status/reset';
    case NODES_NODE_QEMU_VMID_STATUS_RESUME = '/nodes/{node}/qemu/{vmid}/status/resume';
    case NODES_NODE_QEMU_VMID_STATUS_SHUTDOWN = '/nodes/{node}/qemu/{vmid}/status/shutdown';
    case NODES_NODE_QEMU_VMID_STATUS_START = '/nodes/{node}/qemu/{vmid}/status/start';
    case NODES_NODE_QEMU_VMID_STATUS_STOP = '/nodes/{node}/qemu/{vmid}/status/stop';
    case NODES_NODE_QEMU_VMID_STATUS_SUSPEND = '/nodes/{node}/qemu/{vmid}/status/suspend';
    case NODES_NODE_QEMU_VMID_CLONE = '/nodes/{node}/qemu/{vmid}/clone';
    case NODES_NODE_QEMU_VMID_CONFIG = '/nodes/{node}/qemu/{vmid}/config';
    case NODES_NODE_QEMU_VMID_FEATURE = '/nodes/{node}/qemu/{vmid}/feature';
    case NODES_NODE_QEMU_VMID_MIGRATE = '/nodes/{node}/qemu/{vmid}/migrate';
    case NODES_NODE_QEMU_VMID_MONITOR = '/nodes/{node}/qemu/{vmid}/monitor';
    case NODES_NODE_QEMU_VMID_MOVE_DISK = '/nodes/{node}/qemu/{vmid}/move_disk';
    case NODES_NODE_QEMU_VMID_PENDING = '/nodes/{node}/qemu/{vmid}/pending';
    case NODES_NODE_QEMU_VMID_RESIZE = '/nodes/{node}/qemu/{vmid}/resize';
    case NODES_NODE_QEMU_VMID_RRD = '/nodes/{node}/qemu/{vmid}/rrd';
    case NODES_NODE_QEMU_VMID_RRDDATA = '/nodes/{node}/qemu/{vmid}/rrddata';
    case NODES_NODE_QEMU_VMID_SENDKEY = '/nodes/{node}/qemu/{vmid}/sendkey';
    case NODES_NODE_QEMU_VMID_SPICEPROXY = '/nodes/{node}/qemu/{vmid}/spiceproxy';
    case NODES_NODE_QEMU_VMID_TEMPLATE = '/nodes/{node}/qemu/{vmid}/template';
    case NODES_NODE_QEMU_VMID_TERMPROXY = '/nodes/{node}/qemu/{vmid}/termproxy';
    case NODES_NODE_QEMU_VMID_UNLINK = '/nodes/{node}/qemu/{vmid}/unlink';
    case NODES_NODE_QEMU_VMID_VNCPROXY = '/nodes/{node}/qemu/{vmid}/vncproxy';
    case NODES_NODE_QEMU_VMID_VNCWEBSOCKET = '/nodes/{node}/qemu/{vmid}/vncwebsocket';

    // todo replication

    // todo scan

    // todo sdn

    // todo services

    case NODES_NODE_STORAGE = '/nodes/{node}/storage';
    case NODES_NODE_STORAGE_STORAGE = '/nodes/{node}/storage/{storage}';
    case NODES_NODE_STORAGE_STORAGE_CONTENT = '/nodes/{node}/storage/{storage}/content';
    case NODES_NODE_STORAGE_STORAGE_CONTENT_VOLUME = '/nodes/{node}/storage/{storage}/content/{volume}';
    case NODES_NODE_STORAGE_STORAGE_FILERESTORE_DOWNLOAD = '/nodes/{node}/storage/{storage}/file-restore/download';
    case NODES_NODE_STORAGE_STORAGE_FILERESTORE_LIST = '/nodes/{node}/storage/{storage}/file-restore/list';
    case NODES_NODE_STORAGE_STORAGE_DOWNLOADURL = '/nodes/{node}/storage/{storage}/download-url';
    case NODES_NODE_STORAGE_STORAGE_PRUNEBACKUPS = '/nodes/{node}/storage/{storage}/prunebackups';
    case NODES_NODE_STORAGE_STORAGE_RRD = '/nodes/{node}/storage/{storage}/rrd';
    case NODES_NODE_STORAGE_STORAGE_RRDDATA = '/nodes/{node}/storage/{storage}/rrddata';
    case NODES_NODE_STORAGE_STORAGE_STATUS = '/nodes/{node}/storage/{storage}/status';
    case NODES_NODE_STORAGE_STORAGE_UPLOAD = '/nodes/{node}/storage/{storage}/upload';

    // todo tasks

    // todo vzdump

    case NODES_NODE_APLINFO = '/nodes/{node}/aplinfo';
    case NODES_NODE_CONFIG = '/nodes/{node}/config';
    case NODES_NODE_DNS = '/nodes/{node}/dns';
    case NODES_NODE_HOSTS = '/nodes/{node}/hosts';
    case NODES_NODE_JOURNAL = '/nodes/{node}/journal';
    case NODES_NODE_MIGRATEALL = '/nodes/{node}/migrateall';
    case NODES_NODE_NETSTAT = '/nodes/{node}/netstat';
    case NODES_NODE_QUERY_URL_METADATA = '/nodes/{node}/query-url-metadata';
    case NODES_NODE_REPORT = '/nodes/{node}/report';
    case NODES_NODE_RRD = '/nodes/{node}/rrd';
    case NODES_NODE_RRDDATA = '/nodes/{node}/rrddata';
    case NODES_NODE_SPICESHELL = '/nodes/{node}/spiceshell';
    case NODES_NODE_STARTALL = '/nodes/{node}/startall';
    case NODES_NODE_STATUS = '/nodes/{node}/status';
    case NODES_NODE_STOPALL = '/nodes/{node}/stopall';
    case NODES_NODE_SUBSCRIPTION = '/nodes/{node}/subscription';
    case NODES_NODE_SYSLOG = '/nodes/{node}/syslog';
    case NODES_NODE_TERMPROXY = '/nodes/{node}/termproxy';
    case NODES_NODE_TIME = '/nodes/{node}/time';
    case NODES_NODE_VERSION = '/nodes/{node}/version';
    case NODES_NODE_VNCSHELL = '/nodes/{node}/vncshell';
    case NODES_NODE_VNCWEBSOCKET = '/nodes/{node}/vncwebsocket';
    case NODES_NODE_WAKEONLAN = '/nodes/{node}/wakeonlan';

    case VERSION = '/version';
}
