<?php declare(strict_types=1);

namespace Judahnator\Proxmox;

enum HttpMethod
{
    case DELETE;
    case GET;
    case POST;
    case PUT;
}