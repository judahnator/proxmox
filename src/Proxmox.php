<?php declare(strict_types=1);

namespace Judahnator\Proxmox;

use Symfony\Component\HttpClient\HttpClient as BaseHttpClient;

final class Proxmox
{
    public readonly HttpClient $httpClient;
    public readonly string $version;

    public function __construct(
        string $hostname,
        string $username,
        string $token_name,
        string $token_value,
        string $realm = 'pve',
        int    $port = 8006,
        bool   $verify = false,
    )
    {
        $this->httpClient = new HttpClient(
            $hostname,
            $port,
            BaseHttpClient::create([
                'headers' => [
                    'Authorization' => "PVEAPIToken=$username@$realm!$token_name=$token_value",
                ],
                'verify_peer' => $verify,
                'verify_host' => $verify,
            ]),
        );

        $this->version = $this->get(ApiPaths::VERSION)['version'];
    }

    public function delete(ApiPaths $endpoint, array $arguments = [], array $body = [])
    {
        return $this->httpClient->request(
            HttpMethod::DELETE,
            self::parseEndpoint($endpoint->value, $arguments),
            $body,
        )['data'];
    }

    public function get(ApiPaths $endpoint, array $arguments = [])
    {
        return $this->httpClient->request(
            HttpMethod::GET,
            self::parseEndpoint($endpoint->value, $arguments),
            [],
        )['data'];
    }

    public function post(ApiPaths $endpoint, array $arguments = [], array $body = [])
    {
        return $this->httpClient->request(
            HttpMethod::POST,
            self::parseEndpoint($endpoint->value, $arguments),
            $body,
        )['data'];
    }

    public function put(ApiPaths $endpoint, array $arguments = [], array $body = [])
    {
        return $this->httpClient->request(
            HttpMethod::PUT,
            self::parseEndpoint($endpoint->value, $arguments),
            $body,
        )['data'];
    }

    private static function parseEndpoint(string $url, array $arguments = []): string
    {
        foreach ($arguments as $argument => $value) {
            $url = str_replace('{' . $argument . '}', (string)$value, $url);
        }
        return $url;
    }
}